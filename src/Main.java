import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    private static int dfsStepCounter = 0;

    public static void main(String... args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter printWriter = new PrintWriter(System.out);
        int amountOfMilestones = Integer.parseInt(bufferedReader.readLine());
        int rootNodeIndex = 0;
        for (int i = 0; i < amountOfMilestones; i++) {
            String[] values = bufferedReader.readLine().split(" ");
            int ID = Integer.parseInt(values[0]);
            int parentID = Integer.parseInt(values[1]);
            if (parentID == -1) rootNodeIndex = ID;
            MilestoneTree.getNode(ID).setParent(MilestoneTree.getNode(parentID));
        }

        dfs(MilestoneTree.getNode(rootNodeIndex));

        int result;
        int amountOfRequsts = Integer.parseInt(bufferedReader.readLine());
        for (int i = 0; i < amountOfRequsts; i++) {
            String[] values = bufferedReader.readLine().split(" ");
            Milestone a = MilestoneTree.getNode(Integer.parseInt(values[0]));
            Milestone b = MilestoneTree.getNode(Integer.parseInt(values[1]));

            if (a.discovered < b.discovered && a.finished > b.finished)
                result = 1;
            else if (a.discovered > b.discovered && a.finished < b.finished)
                result = 2;
            else result = 0;

            printWriter.println(result);
        }
        printWriter.flush();
    }

    private static void dfs(Milestone node) {
        node.discovered = dfsStepCounter++;
        for (Milestone m : node.children)
            dfs(m);
        node.finished = dfsStepCounter++;
    }
}

class Milestone {
    public ArrayList<Milestone> children = new ArrayList<>();
    public int discovered;
    public int finished;
    public int index;

    public Milestone(int index) {
        this.index = index;
    }

    public void setParent(Milestone parent) {
        parent.children.add(this);
    }
}

class MilestoneTree {
    public static HashMap<Integer, Milestone> milestones = new HashMap<>();

    public static Milestone getNode(int index) {
        if (!milestones.containsKey(index))
            milestones.put(index, new Milestone(index));
        return milestones.get(index);
    }
}